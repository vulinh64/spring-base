package com.vulinh.data.projection;

import com.vulinh.data.entity.WithDisplayName;
import com.vulinh.utils.StringIdentifiable;

public interface CategoryProjection extends StringIdentifiable, WithDisplayName {}
