package com.vulinh.data.dto.post;

import java.io.Serializable;

public record CategoryDTO(String id, String displayName) implements Serializable {}
