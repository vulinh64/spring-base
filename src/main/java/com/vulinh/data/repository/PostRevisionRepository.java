package com.vulinh.data.repository;

import com.vulinh.data.entity.PostRevision;
import com.vulinh.data.entity.PostRevisionId;

public interface PostRevisionRepository extends BaseRepository<PostRevision, PostRevisionId> {}
