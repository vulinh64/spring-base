package com.vulinh.data.repository;

import com.vulinh.data.entity.Roles;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends BaseRepository<Roles, String> {}
