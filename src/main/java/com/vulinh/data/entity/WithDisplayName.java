package com.vulinh.data.entity;

public interface WithDisplayName {

  String getDisplayName();
}
