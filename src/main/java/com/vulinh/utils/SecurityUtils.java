package com.vulinh.utils;

import com.vulinh.data.dto.security.CustomAuthentication;
import com.vulinh.data.dto.user.UserBasicDTO;
import com.vulinh.exception.ExceptionBuilder;
import jakarta.servlet.http.HttpServletRequest;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Slf4j
public class SecurityUtils {

  private static final String BEGIN_PUBLIC_KEY = "-----BEGIN PUBLIC KEY-----";
  private static final String END_PUBLIC_KEY = "-----END PUBLIC KEY-----";
  private static final String BEGIN_RSA_PUBLIC_KEY = "-----BEGIN RSA PUBLIC KEY-----";
  private static final String END_RSA_PUBLIC_KEY = "-----END RSA PUBLIC KEY-----";

  private static final String BEGIN_PRIVATE_KEY = "-----BEGIN PRIVATE KEY-----";
  private static final String END_PRIVATE_KEY = "-----END PRIVATE KEY-----";
  private static final String BEGIN_RSA_PRIVATE_KEY = "-----BEGIN RSA PRIVATE KEY-----";
  private static final String END_RSA_PRIVATE_KEY = "-----END RSA PRIVATE KEY-----";

  private static final KeyFactory KEY_FACTORY;

  static {
    try {
      KEY_FACTORY = KeyFactory.getInstance("RSA");
      log.info("Using RSA as default algorithm for KeyFactory");
    } catch (Exception exception) {
      log.info("Initializing key factory failed", exception);
      throw ExceptionBuilder.invalidCredentials();
    }
  }

  private static PrivateKey singletonPrivateKey;

  @SneakyThrows
  public static PublicKey generatePublicKey(String rawPublicKey) {
    // Remove header and footer if they are present
    // Also remove any whitespace character (\n, \r, space)
    var refinedPrivateKey =
        StringUtils.deleteWhitespace(
            rawPublicKey
                .replace(BEGIN_PUBLIC_KEY, StringUtils.EMPTY)
                .replace(END_PUBLIC_KEY, StringUtils.EMPTY)
                .replace(BEGIN_RSA_PUBLIC_KEY, StringUtils.EMPTY)
                .replace(END_RSA_PUBLIC_KEY, StringUtils.EMPTY));

    var keySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(refinedPrivateKey));

    return KEY_FACTORY.generatePublic(keySpec);
  }

  @SneakyThrows
  public static PrivateKey generatePrivateKey(String rawPrivateKey) {
    if (singletonPrivateKey == null) {
      // Remove header and footer if they are present
      // Also remove any whitespace character (\n, \r, space)
      var refinedPrivateKey =
          StringUtils.deleteWhitespace(
              rawPrivateKey
                  .replace(BEGIN_PRIVATE_KEY, StringUtils.EMPTY)
                  .replace(END_PRIVATE_KEY, StringUtils.EMPTY)
                  .replace(BEGIN_RSA_PRIVATE_KEY, StringUtils.EMPTY)
                  .replace(END_RSA_PRIVATE_KEY, StringUtils.EMPTY));

      var keySpec = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(refinedPrivateKey));

      singletonPrivateKey = KEY_FACTORY.generatePrivate(keySpec);
    }

    return singletonPrivateKey;
  }

  public static Optional<UserBasicDTO> getUserDTO(@Nullable HttpServletRequest httpServletRequest) {
    return Optional.ofNullable(httpServletRequest)
        .map(HttpServletRequest::getUserPrincipal)
        .or(
            () ->
                Optional.ofNullable(SecurityContextHolder.getContext())
                    .map(SecurityContext::getAuthentication))
        .filter(CustomAuthentication.class::isInstance)
        .map(CustomAuthentication.class::cast)
        .map(CustomAuthentication::getUserDTO);
  }

  @NonNull
  public static UserBasicDTO getUserDTOOrThrow(HttpServletRequest httpServletRequest) {
    return getUserDTO(httpServletRequest).orElseThrow(ExceptionBuilder::invalidAuthorization);
  }
}
